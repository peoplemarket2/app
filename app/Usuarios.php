<?php
    namespace App;

    use Illuminate\Auth\Authenticatable;
    use Laravel\Lumen\Auth\Authorizable;
    use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
    use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

    use Illuminate\Database\Eloquent\Model;
    use Tymon\JWTAuth\Contracts\JWTSubject;

    class Usuarios extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject  {
        use Authenticatable, Authorizable;

        protected $table = 'usuarios';
        protected $primaryKey = 'Documento';
        protected $fillable = ['nombre','apellido','celular','correo','direccion','sexo','usuario'];
        protected $hiden = 'contraseña';

        public function getJWTIdentifier()
        {
            return $this->getKey();
        }

         /**
         * Return a key value array, containing any custom claims to be added to the JWT.
         *
         * @return array
         */
        public function getJWTCustomClaims()
        {
            return [];
        }

    }



?>
