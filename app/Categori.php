<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Categori extends Model
{
    protected $table = 'Categorias';
    protected $primaryKey = 'id';
    protected $fillable = ['id','nombre','descripcion'];
    protected $hidden = ['created_at','updated_at',];
}
?>
