<?php namespace App\Http\Controllers;
use App\Usuarios;
use Illuminate\http\Request;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller{

    public function index(){
        $usuarios = Usuarios::all();
        return response()->json($usuarios,200);
    }

    public function getUsuarios($id){
        $usuario = Usuarios::find($id);
        if($usuario){
            return response()->json($usuario,200);
        }
        return response()->json(['usuario no encontrada'],404);
    }

    public function createUsuarios(Request $request){
        $usuario = new Usuarios;
        $usuario->Documento=$request->Documento;
        $usuario->nombre=$request->nombre;
        $usuario->apellido=$request->apellido;
        $usuario->celular=$request->celular;
        $usuario->correo=$request->correo;
        $usuario->direccion=$request->direccion;
        $usuario->sexo=$request->sexo;
        $usuario->usuario=$request->usuario;
        $usuario->contraseña=$request->contraseña;
        $usuario->save();
        return response()->json($usuario,200);
    }

    public function updateUsuarios(Request $request, $id){
        $usuario = Usuarios::find($id);
        $usuario->nombre=$request->nombre;
        $usuario->apellido=$request->apellido;
        $usuario->celular=$request->celular;
        $usuario->correo=$request->correo;
        $usuario->direccion=$request->direccion;
        $usuario->sexo=$request->sexo;
        $usuario->usuario=$request->usuario;
        $usuario->contraseña=$request->contraseña;
        $usuario->save();
        return response()->json($usuario);
    }

    public function destroyUsuarios($id){
        $usuario = Usuarios::find($id);
        $usuario->delete();
        return response()->json('La usuario fue eliminada');
    }

    public function cambio(Request $request, $id){
        $usuario = Usuarios::find($id);
        $lastpass = $request->lastpass;
        $newpass = $request->newpass;

        if($usuario->contraseña == $lastpass){
            $usuario->contraseña=$newpass;
            $usuario->save();
            return response()->json($usuario,200);
        }else{
            return response()->json(['la contraseña es incorrecta'],401);
        }
    }


    public function login(Request $request){
        $user= $request->usuario;
        $pass= $request->contraseña;
        $usuario = DB::table('usuarios')->where('usuario', $user)->where('contraseña',$pass)->get();
        if(count($usuario) > 0){
            return response()->json([true],200);
        }else{
            return response()->json([false],200);
        }
    }

    public function validUser($user){
        $usuario = DB::table('usuarios')->where('usuario',$user)->get();
        if(count($usuario) > 0){
            return response()->json([true],200);
        }else{
            return response()->json([false],200);
        }
    }

}

?>
