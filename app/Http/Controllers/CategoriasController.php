<?php namespace App\Http\Controllers;
use App\Categori;
use Illuminate\http\Request;

class CategoriasController extends Controller
{
    public function index()
    {
        $categorias = Categori::all();
        return response()->json($categorias,200);
    }

    public function createCategoria(Request $request){
        $categoria = new Categori();
        $categoria->nombre=$request->nombre;
        $categoria->descripcion=$request->descripcion;
        $categoria->save();
        return response()->json($categoria,200);
    }

}
