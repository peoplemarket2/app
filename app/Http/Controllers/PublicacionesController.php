<?php namespace App\Http\Controllers;
use App\Publicaciones;
use Illuminate\http\Request;
use Illuminate\Support\Facades\DB;

class PublicacionesController extends Controller
{
    public function index()
    {
        $publicaciones = Publicaciones::all();
        return response()->json($publicaciones,200);
    }

    public function getPub($id){
        $publicacion = Publicaciones::find($id);
        if($publicacion){
            return response()->json($publicacion,200);
        }
        return response()->json(['publicacion no encontrada'],404);
    }

    public function createPublicacion(Request $request){
        if($request->diferente == null){
            $publicacion = new Publicaciones();
            $publicacion->producto=$request->producto;
            $publicacion->marca=$request->marca;
            $publicacion->color=$request->color;
            $publicacion->celular=$request->celular;
            $publicacion->metodoPago=$request->metodoPago;
            $publicacion->descripcion=$request->descripcion;
            $publicacion->precio=$request->precio;
            $publicacion->tipoProducto=$request->tipoProducto;
            $publicacion->imagenes=$request->imagenes;
            $publicacion->estado='D';
        }else{
            $publicacion = new Publicaciones();
            $publicacion->producto=$request->producto;
            $publicacion->marca=$request->marca;
            $publicacion->color=$request->color;
            $publicacion->celular=$request->celular;
            $publicacion->metodoPago=$request->metodoPago;
            $publicacion->descripcion=$request->descripcion;
            $publicacion->precio=$request->precio;
            $publicacion->tipoProducto=$request->diferente;
            $publicacion->imagenes=$request->imagenes;
            $publicacion->estado='D';

        }

        $publicacion->save();
        return response()->json($publicacion,200);
    }

    public function inhabilitar(Request $request, $id){
        $publicacion = Publicaciones::find($id);
        $publicacion->producto=$request->producto;
        $publicacion->marca=$request->marca;
        $publicacion->color=$request->color;
        $publicacion->celular=$request->celular;
        $publicacion->metodoPago=$request->metodoPago;
        $publicacion->descripcion=$request->descripcion;
        $publicacion->precio=$request->precio;
        $publicacion->tipoProducto=$request->tipoProducto;
        $publicacion->imagenes=$request->imagenes;
        $publicacion->estado=$request->estado;
        $publicacion->save();
        return response()->json($publicacion);
    }

    public function actualizarPublicacion(Request $request, $id){
        $publicacion = Publicaciones::find($id);
        $publicacion->producto=$request->producto;
        $publicacion->marca=$request->marca;
        $publicacion->color=$request->color;
        $publicacion->celular=$request->celular;
        $publicacion->metodoPago=$request->metodoPago;
        $publicacion->descripcion=$request->descripcion;
        $publicacion->precio=$request->precio;
        $publicacion->tipoProducto=$request->tipoProducto;
        $publicacion->imagenes=$request->imagenes;
        $publicacion->save();
        return response()->json($publicacion);
    }
}
