-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2020 a las 23:22:53
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `market`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `updated_at`, `created_at`) VALUES
(1, 'TECNOLOGIA', 'EQUIPOS DE COMPUTO, MOVILES, CONSOLAS', NULL, NULL),
(2, 'HOGAR', 'COCINA, CLOSET', '2020-05-17 23:03:10', '2020-05-17 23:03:10'),
(3, 'MONTAÑISMO', 'VEHICULOS, DEPORTE, CICLAS', '2020-05-17 23:11:56', '2020-05-17 23:11:56'),
(4, 'ASEO', 'IMPLEMENTOSa', '2020-05-17 23:13:41', '2020-05-17 23:13:41'),
(5, 'VEHICULOS', 'TRANSPORTE DE PERSONAS', '2020-05-18 01:35:10', '2020-05-18 01:35:10'),
(6, 'APARTAMENTOS', 'APARTAMENTOS DE 50 X 60 ', '2020-05-18 13:27:33', '2020-05-18 13:27:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2020_05_09_034045_create_usuarios_table', 1),
(3, '2020_05_17_055728_create_publicaciones_table', 2),
(5, '2020_05_17_222116_create_categorias_table', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE `publicaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metodoPago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipoProducto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagenes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`id`, `producto`, `marca`, `color`, `celular`, `metodoPago`, `descripcion`, `precio`, `tipoProducto`, `imagenes`, `updated_at`, `created_at`, `estado`) VALUES
(17, 'PORTATIL 27.4\" ', 'ASUS', 'DORADO', '3123196836', 'Efectivo', 'PROCESADOR CORE I5 8650\nRAM 12 GB\nDD 1TB\nTARJETA GRAFICA NVDIA', '1800000', 'TECNOLOGIA', 'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQ93m-rz533xRn8X35j4uhAy7TXI6-Z0hop5qjWL-wPQNoOnbz7RtA&usqp=CAc', '2020-05-24 13:17:50', '2020-05-18 11:53:19', 'D'),
(18, 'TELEVISOR 52\"', 'SAMSUNG', 'NEGRO', '3123196836', 'credito', 'SMART TV\nULTRA HD\nSONIDO ENVOLVENTE', '3000000', 'TECNOLOGIA', 'https://images.samsung.com/is/image/samsung/ar-uhdtv-mu6100-un50mu6100gxzd-black-136495500?$PD_GALLERY_L_JPG$', '2020-05-24 02:04:09', '2020-05-18 12:01:36', 'V'),
(19, 'SMART PHONE', 'MOTOROLA', 'NEGRO', '3123196836', 'Efectivo', 'RAM 4GB\nMEMORIA 8GB\nTAMAÑO 4\"', '450000', 'TECNOLOGIA', 'https://i.blogs.es/2989e5/portrada/1366_2000.jpg', '2020-05-23 21:11:00', '2020-05-18 13:23:07', 'V'),
(20, 'portatil', 'dell', 'negro', '3123196836', 'credito', 'super rapido de alto rendimiento', '1250000', 'TECNOLOGIA', 'https://pcware.com.co/wp-content/uploads/2018/08/portatil-dell-vostro-3468-empresarial_1_700x700-700x700.jpg', '2020-05-20 14:01:05', '2020-05-20 14:01:05', 'D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contraseña` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Documento`, `nombre`, `apellido`, `celular`, `correo`, `direccion`, `sexo`, `usuario`, `contraseña`, `updated_at`, `created_at`) VALUES
('1086137767', 'yenny', 'miramag', '3146098716', 'yenny.miramag@gmail.com', 'no me la sé', 'mujer', 'yenny', '111111111', '2020-05-24 02:06:17', '2020-05-24 02:06:17'),
('1101760015', 'ingrith', 'santamaria', '321588065', 'ingrithsanta@gmail.com', 'carrera 11 D 45 31', 'mujer', 'sehor', '81943301Se', '2020-05-23 22:06:26', '2020-05-23 22:06:26'),
('1144201161', 'Sebastian', 'Ricon', '3123196836', 'sebas199765@gmail.com', 'Carrera 11D #45-31', 'hombre', 'Arturoa', '819443301Se', '2020-05-24 20:59:23', '2020-05-24 20:59:23'),
('2156484', 'Ingrith', 'Santamaria', '3123196836', 'ingrithsanta@gmail.com', 'Carrera 11D #45-31', 'mujer', 'SouthCraft', '81943301', '2020-05-11 23:53:31', '2020-05-11 02:58:31');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Documento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
